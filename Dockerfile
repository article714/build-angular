
ARG NODE_VERSION
ARG DEBIAN_VERSION
FROM node:${NODE_VERSION}-${DEBIAN_VERSION}
LABEL maintainer="C. Guychard<christophe@article714.org>"

ARG ANGULAR_VERSION

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

# Path to node & ng-cli

ENV NPM_CONFIG_PREFIX=/home/builder/angular-base-deps
ENV NODE_PATH=/home/builder/angular-base-deps/lib/node_modules
ENV PATH=$PATH:/home/builder/angular-base-deps/bin:/usr/local/bin


# Container tooling

COPY container /container

# container building

RUN /container/build.sh

USER builder
